﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserAPI.Model;
using UserAPI.Service;

namespace UserAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IuserService _userService;
        public UserController(IuserService userService)
        {
            _userService = userService;
        }
        [Route("GetAllUsers")]
        [HttpGet]
        public ActionResult GetAllUser()
        {
            List<User> users = _userService.GetAlluser();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            bool registerUserStatus = _userService.RegisterUser(user);
            return Ok(registerUserStatus);
        }
        [Route("DeletUser/{id:int}")]
        public ActionResult DeletUser(int id)
        {
            bool userDeleteStatus = _userService.DeletUser(id);
            return Ok(userDeleteStatus);
        }
        [Route("DeletUser/{id:int}")]
        [HttpGet]
        public ActionResult GetUserById(int id)
        {
            User user = _userService.GetUserById(id);
            return Ok(user);
        }
        [Route("EditUser/{id:int}")]
        [HttpPost]
        public ActionResult EditUser(int id,User user)
        {
            bool editUserStatus = _userService.EditUser(id, user);
            return Ok(editUserStatus);  
        }
        [Route("BlockUnBlockUser")]
        [HttpPut]
        public ActionResult BlockUnBlockUser(int id,bool blockUnblockUser)
        {
            bool blockUnblockStatus = _userService.BlockUnBlockUser(id, blockUnblockUser);
            return Ok(blockUnblockStatus);
        }

        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(LoginUser loginUser)
        {
            User user = _userService.LogIn(loginUser);
            return Ok(user);
        }
    }
}
